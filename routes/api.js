const express = require('express');
const router = express.Router();
const Pg = require('../models/pg');
const Ekm = require('../models/ekm');

//get Request
router.get('/pg', function (req, res, next){
    Pg.find(function (err, docs){
         if (err){
             res.json(err);
         }
         else {
              res.json(docs);
         }
    });
});


// get reqquest two 
router.get('/ekm', function (req, res, next){
     Ekm.find(function (err, docs){
            if (err){
                res.json(err);
            }
            else {
                res.json(docs);
            }
     });
});



module.exports = router;