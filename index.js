const express = require('express');
const bodyParser = require('body-parser');
const mogoose = require('mongoose');
const routes = require('./routes/api');

// set up express app
const app = express();

//connect to mongodb
mogoose.connect('mongodb://localhost:27017/dummy',{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
   .then((res)=> {
          console.log('Connected eastablished successfully');

   })
   .catch((err)=>{
          console.log(`ERRor while CONNecting to db ${err}`);
   });
    // parser
   app.use(bodyParser.urlencoded({ extended: false }));
  
   //body parser
   app.use(bodyParser.json());

   //initialize routes
   app.use('/api', require('./routes/api'));

   //error handling midleware
   app.use(function(err, req, res, next){
       //console.log(err);
       res.status(422).send({error: err.message});
   });



   //listen for requests
   app.listen(process.env.port ||3001, function(){
       console.log('now listening for requests');
   });