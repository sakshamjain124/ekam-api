const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create PG Schema & Model
const PgSchema = new Schema({

        bugName: String,
        bugColour: String,
        Genus: String,
        bugGender:String,
        bugAge:String
});


Pg = mongoose.model("Pg", PgSchema)
    Pg = new Pg({
        bugName: "Scruffy",
        bugColour: "Orange",
        Genus: "Bombus",
        bugGender: "male",
        bugAge: "58"
    });
    
    Pg.save(function(error) {
        console.log("Your PG has been saved!");
    if (error) {
        console.error(error);
     }
   });


   module.exports = mongoose.model('pg', PgSchema);

// const Pg = mongoose.model('pg', PgSchema);

// module.exports = Pg;s